const { Datastore } = require('@google-cloud/datastore');
const { readFile, writeFile } = require('fs').promises;
const express = require('express');
const cors = require('cors');
const e = require('express');
const fs = require('fs');

const app = express();
app.use(express.json());
app.use(cors());

const datastore = new Datastore();

// App Engine server link: https://town-hall-service.ue.r.appspot.com/

// =======================================================
// Issue-Analysis Service
// dateOfIssue: date of happening
// dateOfPosted, description, location, type, isReviewed, isHighlighted

// GET all issues
app.get('/issues', async (req, res) => {
    if (req.query.type) {
        // if issue type was asked for
        // GET /issues?type=something
        const query = datastore.createQuery('Issue').filter('type', '=', req.query.type);
        const [data, metaInfo] = await datastore.runQuery(query);
        const issues = data.map((i, index) => {
            return { issue: i, id: data[index][datastore.KEY]['id'] }
        });
        res.send(issues);
    } else if (req.query.dateposted) {
        // if date of posted was asked for
        // GET /issues?dateposted=somedate
        const query = datastore.createQuery('Issue').filter('dateOfPosted', '=', req.query.dateposted);
        const [data, metaInfo] = await datastore.runQuery(query);
        const issues = data.map((i, index) => {
            return { issue: i, id: data[index][datastore.KEY]['id'] }
        });
        res.send(issues);
    } else if (req.query.dateofissue) {
        // if date of issue was asked for
        // GET /issues?dateofissue=somedate
        const query = datastore.createQuery('Issue').filter('dateOfIssue', '=', req.query.dateofissue);
        const [data, metaInfo] = await datastore.runQuery(query);
        const issues = data.map((i, index) => {
            return { issue: i, id: data[index][datastore.KEY]['id'] }
        });
        res.send(issues);
    } else if (req.query.reviewed) {
        // if reviewing status was asked for
        // GET /issues?reviewed=true
        if (String(req.query.reviewed) == 'true') {
            const query = datastore.createQuery('Issue').filter('isReviewed', '=', true);
            const [data, metaInfo] = await datastore.runQuery(query);
            const issues = data.map((i, index) => {
                return { issue: i, id: data[index][datastore.KEY]['id'] }
            });
            res.send(issues);
        } else if (String(req.query.reviewed) == 'false') {
            const query = datastore.createQuery('Issue').filter('isReviewed', '=', false);
            const [data, metaInfo] = await datastore.runQuery(query);
            const issues = data.map((i, index) => {
                return { issue: i, id: data[index][datastore.KEY]['id'] }
            });
            res.send(issues);
        } else {
            res.send('Invalid value of req.query.reviewed. Query should be true/false.');
        }
    } else if (req.query.highlighted) {
        // if highlight status was asked for
        // GET /issues?highlighted=true
        if (String(req.query.highlighted) == 'true') {
            const query = datastore.createQuery('Issue').filter('isHighlighted', '=', true);
            const [data, metaInfo] = await datastore.runQuery(query);
            const issues = data.map((i, index) => {
                return { issue: i, id: data[index][datastore.KEY]['id'] }
            });
            res.send(issues);
        }
        else if (String(req.query.highlighted) == 'false') {
            const query = datastore.createQuery('Issue').filter('isHighlighted', '=', false);
            const [data, metaInfo] = await datastore.runQuery(query);
            const issues = data.map((i, index) => {
                return { issue: i, id: data[index][datastore.KEY]['id'] }
            });
            res.send(issues);
        } else {
            res.send('Invalid value of req.query.highlighted. Query should be true/false.');
        }
    } else {
        // if no queries were asked for, then get all issues from the database
        const query = datastore.createQuery('Issue');
        const [data, metaInfo] = await datastore.runQuery(query);
        const issues = data.map((i, index) => {
            return { issue: i, id: data[index][datastore.KEY]['id'] }
        });
        res.send(issues);
    }
});
// gets the count of an array of issues by type
function getCount(arr, type) {
    let count = 0;
    for (let i of arr) {
        if (i.type === type) {
            count += 1;
        }
    }
    return count;
}
// gets the count of highlighted/reviewed issues based on boolean check
function getReviewedCount(arr, check) {
    let count = 0;
    for (let i of arr) {
        if (i.isReviewed === check) {
            count += 1;
        }
    }
    return count;
}

app.get('/issues/report', async (req, res) => {
    // --------------------------------------------------------------
    // gets the count of infrastructure issues
    const queryOfInfrastructure = datastore.createQuery('Issue').filter('type', '=', 'Infrastructure');
    const [dataOfInfrastructure] = await datastore.runQuery(queryOfInfrastructure);
    const countOfInfrastructure = dataOfInfrastructure.length;
    // --------------------------------------------------------------
    // gets the count of safety issues
    const queryOfSafety = datastore.createQuery('Issue').filter('type', '=', 'Safety');
    const [dataOfSafety] = await datastore.runQuery(queryOfSafety);
    const countOfSafety = dataOfSafety.length;
    // --------------------------------------------------------------
    // gets the count of public health issues
    const queryofPublicHealth = datastore.createQuery('Issue').filter('type', '=', 'Public Health');
    const [dataOfPublicHealth] = await datastore.runQuery(queryofPublicHealth);
    const countOfPublicHealth = dataOfPublicHealth.length;
    // --------------------------------------------------------------
    // gets the count of pollution issues
    const queryOfPollution = datastore.createQuery('Issue').filter('type', '=', 'Pollution');
    const [dataOfPollution] = await datastore.runQuery(queryOfPollution);
    const countOfPollution = dataOfPollution.length;
    // --------------------------------------------------------------
    // gets the count of Noise/Disturbing issues
    const queryOfNoiseDisturbing = datastore.createQuery('Issue').filter('type', '=', 'Noise/Disturbing the peace');
    const [dataOfNoiseDisturbing] = await datastore.runQuery(queryOfNoiseDisturbing);
    const countOfNoiseDisturbing = dataOfNoiseDisturbing.length;
    // --------------------------------------------------------------
    // gets the count of ther issues
    const queryOfOther = datastore.createQuery('Issue').filter('type', '=', 'Other');
    const [dataOfOther] = await datastore.runQuery(queryOfOther);
    const countOfOther = dataOfOther.length;
    // --------------------------------------------------------------
    // gets the count of issues that are reviewed
    const queryOfReviewedIssues = datastore.createQuery('Issue').filter('isReviewed', '=', true);
    const [dataOfReviewedIssues] = await datastore.runQuery(queryOfReviewedIssues);
    const countOfReviewedIssues = dataOfReviewedIssues.length;
    // --------------------------------------------------------------
    // gets the count of issues that arent reviewed
    const queryOfPendingReviewedIssues = datastore.createQuery('Issue').filter('isReviewed', '=', false);
    const [dataOfPendingReviewedIssues] = await datastore.runQuery(queryOfPendingReviewedIssues);
    const countOfPendingReviewedIssues = dataOfPendingReviewedIssues.length;

    // --------------------------------------------------------------
    // number of issues reported in the last 24 hours, broken down by type
    const d = new Date();         // today
    let d24hrs = new Date();
    d24hrs.setDate(d24hrs.getDate() - 1); // 24 hrs ago
    const queryOfIssues24hrs = datastore.createQuery('Issue').filter('datePosted', '>=', d24hrs).filter('datePosted', '<=', d);
    const [dataOfIssues24hrs] = await datastore.runQuery(queryOfIssues24hrs);
    // sets up an object to store the total counts of all the issues within 24 hours
    let twoFourHours = { numberOfIssues: 0 }
    if (dataOfIssues24hrs.length > 0) {
        twoFourHours.numberOfIssues = dataOfIssues24hrs.length;
        twoFourHours.infrastructure = getCount(dataOfIssues24hrs, 'Infrastructure');
        twoFourHours.safety = getCount(dataOfIssues24hrs, 'Safety');
        twoFourHours.publicHealth = getCount(dataOfIssues24hrs, 'Public Health');
        twoFourHours.pollution = getCount(dataOfIssues24hrs, 'Pollution');
        twoFourHours.noiseDisturbing = getCount(dataOfIssues24hrs, 'Noise/Disturbing the peace');
        twoFourHours.other = getCount(dataOfIssues24hrs, 'Other');
        twoFourHours.reviewed = getReviewedCount(dataOfIssues24hrs, true);
        twoFourHours.awaitingReviewal = getReviewedCount(dataOfIssues24hrs, false);
    }

    // --------------------------------------------------------------
    // number of issues reported in the last 7 days, broken down by type
    let d7 = new Date();
    d7.setDate(d7.getDate() - 7); // 7 days ago
    const queryOfIssues7days = datastore.createQuery('Issue').filter('datePosted', '>', d7).filter('datePosted', '<=', d);
    const [dataOfIssues7Days] = await datastore.runQuery(queryOfIssues7days);
    // sets up an object to store the total counts of all the issues within 7 days
    let sevenDays = { numberOfIssues: 0 };
    if (dataOfIssues7Days.length > 0) {
        sevenDays.numberOfIssues = dataOfIssues7Days.length;
        sevenDays.infrastructure = getCount(dataOfIssues7Days, 'Infrastructure');
        sevenDays.safety = getCount(dataOfIssues7Days, 'Safety');
        sevenDays.publicHealth = getCount(dataOfIssues7Days, 'Public Health');
        sevenDays.pollution = getCount(dataOfIssues7Days, 'Pollution');
        sevenDays.noiseDisturbing = getCount(dataOfIssues7Days, 'Noise/Disturbing the peace');
        sevenDays.other = getCount(dataOfIssues7Days, 'Other');
        sevenDays.reviewed = getReviewedCount(dataOfIssues7Days, true);
        sevenDays.awaitingReviewal = getReviewedCount(dataOfIssues7Days, false);
    }

    // ========================================
    // Returns a single JSON that contains summarized information
    const report = {
        infrastructure: countOfInfrastructure,
        safety: countOfSafety,
        publicHealth: countOfPublicHealth,
        pollution: countOfPollution,
        noiseDisturbing: countOfNoiseDisturbing,
        other: countOfOther,
        last24Hours: twoFourHours,
        last7Days: sevenDays,
        reviewed: countOfReviewedIssues,
        awaitingReviewal: countOfPendingReviewedIssues
    }
    res.send(report);

    // ========================================
    // generates report.txt file that has the information
    const path = './logs/report.txt';

    if (fs.existsSync(path) == false) {
        // if report.txt file DNE, then create a new empty one
        await writeFile(path, '');
    }
    // sends report data to file
    const fileData = await readFile(path);
    const info = fileData.toString();
    const updateInfo = `${info} 
Statistics as of ${d} : ${JSON.stringify(report)}`;

    await writeFile(path, updateInfo);
});

// reset report.txt file
app.get('/reset', async (req, res) => {
    await writeFile('./logs/report.txt', '');
});

// display report.txt file
app.get('/reportfile', async (req, res) => {
    const fileData = await readFile('./logs/report.txt');
    const info = fileData.toString();
    res.send(info);
});

// needed to be able to update an issue if isHighlighted or isReviewed changed
app.put('/issues/:id', async (req, res) => {
    try {
        const key = datastore.key(['Issue', Number(req.params.id)]);
        const data = req.body;
        await datastore.update({ key: key, data: data })
        res.status(200).send(`Issue with id ${req.params.id} updated.`)
    } catch (error) {
        res.status(404).send(`Issue with id ${req.params.id} does not exist.`)
    }
})

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Server started on PORT " + PORT));